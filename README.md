## 内容
reduxのstoreをサーバー上に持って、みんなで共有してみる。
そんなサンプルです。

## 使い方
### server

```shell
# トランスパイルを呼び出すのにタスクランナーとしてgulpを用いる
# global, local共にインストールされている必要がある。
$ sudo npm install gulp -g

$ cd ./server
$ npm install
$ gulp babel

$ node build/server.js
```

### client
```shell
$ cd ./server
$ npm install

# LISTENするポート番号はご自由に
$ PORT=8080 npm start
```
