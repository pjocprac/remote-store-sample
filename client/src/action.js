import RemoteStore from './remote_store';

export function updateRemoteState(payload) {
  return {
    type: 'UPDATE_REMOTE_STATE',
    payload,
  };
}

export function login(username) {
  const remoteStore = RemoteStore.getInstance();
  remoteStore.connect();
  remoteStore.dispatch('LOGIN', username)

  return {type: 'LOGIN'}
}

export function logout() {
  const remoteStore = RemoteStore.getInstance();
  remoteStore.dispatch('LOGOUT', null);
  remoteStore.connect();

  return {type: 'LOGOUT'}
}

export function postMessage(message) {
  const remoteStore = RemoteStore.getInstance();
  remoteStore.dispatch('POST_MESSAGE', message);

  return {type: 'POST_MESSAGE'};
}

export function changeLoginUserName(value) {
  return {type: 'CHANGE_LOGIN_USER_NAME', value}
}

export function changePostMessageText(value) {
  return {type: 'CHANGE_POST_MESSAGE_TEXT', value}
}
