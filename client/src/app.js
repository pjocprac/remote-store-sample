import React from 'react';
import { connect } from 'react-redux'

import * as action from './action';

class App extends React.Component {

  /**
   * エンターキーが押された際のログインハンドリング
   */
  handleLogin(e) {
    const {loginUserName} = this.props;
    if (e.keyCode === 13 && loginUserName !== '') {
      this.props.login(loginUserName)
    }
  }

  /**
   * エンターキーが押された際の投稿ハンドリング
   */
  handlePostMessage(e) {
    const {postMessageText} = this.props;
    if (e.keyCode === 13 && postMessageText !== '') {
      this.props.postMessage(postMessageText)
    }
  }

  renderLogin() {
    if (this.props.isLoggedIn) {
      return null;
    }

    const {loginUserName} = this.props;
    return (
      <div className="center">
        <div className="login">
          <input
            className="input"
            type="text"
            value={loginUserName}
            onChange={e => this.props.changeLoginUserName(e.target.value)}
            onKeyDown={e => this.handleLogin(e)}
          />
          <input
            className="button"
            type="button"
            value="ログイン"
            disabled={loginUserName === ''}
            onClick={() => this.props.login(loginUserName)}
          />
        </div>
      </div>
    );
  }

  renderThread() {
    if (!this.props.isLoggedIn) {
      return null;
    }

    const {postMessageText} = this.props;
    let messages = this.props.remoteState.messages || [];
    messages = messages.concat([]);
    messages.reverse();

    let loginUsers = this.props.remoteState.loginUsers || [];

    return (
      <div className="thread">
        <div className="messages">
          <div className="messages-inner">
            {
              messages.map((msgdata, i) => (
                <div className="message" key={i}>
                  <div className="name">{msgdata.username}</div>
                  <div className="text">{msgdata.message}</div>
                </div>
              ))
            }
          </div>
        </div>
        <div className="post">
          <input
            className="input"
            type="text"
            value={postMessageText}
            onChange={e => this.props.changePostMessageText(e.target.value)}
            onKeyDown={e => this.handlePostMessage(e)}
          />
          <input
            className="button"
            type="button"
            value="発言"
            disabled={postMessageText === ''}
            onClick={() => this.props.postMessage(postMessageText)}
          />
          <div className="users">
            ログイン中 (
            {Object.values(loginUsers).map(user => user).join(', ')}
            )
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="app">
        {this.renderLogin()}
        {this.renderThread()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state
}

export default connect(mapStateToProps, action)(App)
