import React from 'react';
import ReactDOM from 'react-dom';

import { createStore } from 'redux'
import { Provider } from 'react-redux'

import App from './app';
import reducer from './reducer';
import RemoteStore from './remote_store';

import './index.css';

const store = createStore(reducer);

// remote stateの更新があった場合にactionをdispatchできるように
// storeを渡してinitializeする。
RemoteStore.init(store);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

// ブラウザを閉じた時にWebSocketをdisconnectする
window.onbeforeunload = function(e) {
  RemoteStore.getInstance().disconnect();
};
