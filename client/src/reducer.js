const initialState = {
  loginUserName: '',
  postMessageText: '',
  isLoggedIn: false,
  remoteState: {},
};

export default (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case 'LOGIN':
      return Object.assign({}, state, {isLoggedIn: true});

    case 'LOGOUT':
      return Object.assign({}, state, {isLoggedIn: false});

    case 'UPDATE_REMOTE_STATE':
      return Object.assign({}, state, {remoteState: action.payload});

    case 'POST_MESSAGE':
      return Object.assign({}, state, {postMessageText: ''});

    case 'CHANGE_LOGIN_USER_NAME':
      return Object.assign({}, state, {loginUserName: action.value});

    case 'CHANGE_POST_MESSAGE_TEXT':
      return Object.assign({}, state, {postMessageText: action.value});

    default:
      return state;
  }
}
