import io from 'socket.io-client';
import * as action from './action';

const url = '192.168.0.104:3001';

export default class RemoteStore {
  static _instance = null;

  constructor(store) {
    this.store = store;
    this.socket = null;
  }

  static init(store) {
    if (!RemoteStore._instance) {
      RemoteStore._instance = new RemoteStore(store);
    }
  }

  static getInstance() {
    return RemoteStore._instance;
  }

  /**
   * WebSocketサーバーに接続し、UPDATE_STATEイベントを待ち受ける
   */
  connect() {
    if (!this.socket) {
      this.socket = io.connect(url);

      this.socket.on('UPDATE_STATE', data => {
        this.store.dispatch(action.updateRemoteState(data));
      });
    }
  }

  /**
   * WebSocketサーバーから切断
   */
  disconnect() {
    if (this.socket) {
      this.socket.disconnect();
    }
  }

  /**
   * リモートのストアにアクションをディスパッチする。
   * 実際には、WebSocketのemitでイベントを送信。
   */
  dispatch(type, payload) {
    if (this.socket) {
      this.socket.emit(type, payload);
    }
  }
}
