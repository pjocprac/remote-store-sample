export function login(payload) {
  return {
    type: 'LOGIN',
    payload,
  }
}

export function postMessage(payload) {
  return {
    type: 'POST_MESSAGE',
    payload,
  }
}

export function logout(payload) {
  return {
    type: 'LOGOUT',
    payload,
  }
}
