const initialState = {
  messages: [],
  loginUsers: {},
};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case 'LOGIN': {
      const {id, username} = action.payload;

      // ログイン中のユーザーを更新
      const loginUsers = Object.assign({}, state.loginUsers, {[id]: username});

      // メッセージ一覧を更新 (ログインメッセージ追加)
      const msgdata = {
        username: '',
        message: `${username}さんがログインしました`
      };
      const messages = state.messages.concat([msgdata]);

      return Object.assign({}, state, {loginUsers, messages});
    }

    case 'LOGOUT': {
      const {id} = action.payload;

      // ログイン中のユーザーを更新
      const loginUsers = Object.assign({}, state.loginUsers);
      delete loginUsers[id];

      // メッセージ一覧を更新 (ログアウトメッセージ追加)
      const username = state.loginUsers[id];
      const msgdata = {
        username: '',
        message: `${username}さんがログアウトしました`
      };
      const messages = state.messages.concat([msgdata]);

      return Object.assign({}, state, {loginUsers, messages});
    }

    case 'POST_MESSAGE': {
      const {id, message} = action.payload;
      const username = state.loginUsers[id];

      // メッセージ一覧を更新
      const msgdata = {username, message};
      const messages = state.messages.concat([msgdata]);

      return Object.assign({}, state, {messages});
    }

    default:
      return state;
  }
}
