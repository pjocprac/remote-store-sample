import http from 'http';
import fs from 'fs';
import socketio from 'socket.io';

import {createStore} from 'redux'

import {reducer} from './reducer';
import * as action from './action';


const server = http.createServer().listen(3001);
console.log('start server @ http://localhost:3001/');

const io = socketio.listen(server);

// Reduxストア初期化
const store = createStore(reducer);

/**
 * アクションをディスパッチし、ステートを更新。
 * 更新されたステートをUPDATE_STATEというイベントで全クライアントに送信する。
 */
function setState(action) {
  store.dispatch(action);
  io.sockets.emit('UPDATE_STATE', store.getState());
}

/*
 * クライアントからのイベントを受けて、アクションを発行。
 */
io.sockets.on('connection', socket => {
  console.log(`connect ${socket.id}`);

  socket.on('LOGIN', username => {
    console.log('LOGIN', socket.id, username);
    setState(action.login({id: socket.id, username}));
  });

  socket.on('POST_MESSAGE', data => {
    console.log('POST_MESSAGE', socket.id, data);
    setState(action.postMessage({id: socket.id, message: data}));
  });

  socket.on('disconnect', data => {
    console.log('disconnect', socket.id);
    setState(action.logout({id: socket.id}));
  });
});
